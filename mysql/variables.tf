variable "kubeconfig_path" {
  description = "Kubeconfig path"
  type        = string
  default     = "~/.kube/config"
}

variable "namespace" {
  description = "Namespace for MySQL deployment"
  type        = string
  default     = "mysql"
}

variable "mysql_root_password" {
  description = "Root password for MySQL"
  type        = string
  default     = "root1184!"
}

variable "mysql_database" {
  description = "Database name to create"
  type        = string
  default     = "mysql_shared"
}

variable "mysql_user" {
  description = "Username of new user to create"
  type        = string
  default     = "mysql_user"
}

variable "mysql_password" {
  description = "Password for the new user"
  type        = string
  default     = "shared1184!"
}

variable "mysql_replica_count" {
  description = "Number of MySQL replicas"
  type        = number
  default     = 1
}

variable "mysql_storage_class" {
  description = "Storage class to use for MySQL PVC"
  type        = string
  default     = ""
}

variable "mysql_pvc_size" {
  description = "Size of MySQL PVC"
  type        = string
  default     = "16Gi"
}

variable "resources" {
  description = "Resource requests and limits for MySQL pods"
  type = object({
    requests = object({
      cpu    = string
      memory = string
    })
    limits = object({
      cpu    = string
      memory = string
    })
  })
  default = {
    requests = {
      cpu    = "100m"
      memory = "256Mi"
    }
    limits = {
      cpu    = "500m"
      memory = "512Mi"
    }
  }
}

variable "node_selector" {
  description = "Node selector for MySQL pods"
  type        = map(string)
  default     = {}
}

variable "tolerations" {
  description = "Tolerations for MySQL pods"
  type        = list(map(string))
  default     = []
}

variable "affinity" {
  description = "Affinity rules for MySQL pods"
  type        = any
  default     = {
    "affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].key" = "kubernetes.io/hostname"
    "affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].operator" = "In"
    "affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values[0]" = "node3"
  }
}
