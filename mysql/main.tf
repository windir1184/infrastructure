resource "kubernetes_namespace" "mysql" {
  metadata {
    name = "mysql"
  }
}

resource "kubernetes_storage_class_v1" "mysql" {
  metadata {
    name = "local-storage"
  }
  storage_provisioner = "kubernetes.io/no-provisioner"
  reclaim_policy      = "Delete"
  volume_binding_mode = "Immediate"
}

resource "kubernetes_persistent_volume_v1" "mysql" {
  metadata {
    name = "local-pv"
  }
  spec {
    storage_class_name = kubernetes_storage_class_v1.mysql.metadata[0].name
    capacity = {
      storage = "32Gi"
    }
    access_modes = [
      "ReadWriteOnce"
    ]
    persistent_volume_source {
      local {
        path = "/opt/data"
      }
    }
    node_affinity {
      required {
        node_selector_term {
          match_expressions {
            key      = "kubernetes.io/hostname"
            operator = "In"
            values   = [
              "node3"
            ]
          }
        }
      }
    }
  }
}

module "mysql" {
  source              = "./modules/bitnami-mysql"
  namespace           = kubernetes_namespace.mysql.metadata[0].name
  mysql_root_password = var.mysql_root_password
  mysql_database      = var.mysql_database
  mysql_user          = var.mysql_user
  mysql_password      = var.mysql_password
  mysql_replica_count = var.mysql_replica_count
  mysql_storage_class = var.mysql_storage_class == "" ? kubernetes_storage_class_v1.mysql.metadata[0].name : var.mysql_storage_class
  mysql_pvc_size      = var.mysql_pvc_size
  node_selector       = var.node_selector
  tolerations         = var.tolerations
  affinity            = var.affinity
}
