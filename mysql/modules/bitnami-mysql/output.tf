output "mysql_root_password" {
  value = var.mysql_root_password
}

output "mysql_database" {
  value = var.mysql_database
}

output "mysql_user" {
  value = var.mysql_user
}

output "mysql_password" {
  value = var.mysql_password
}

output "release" {
  description = "Block status of the deployed release."
  value       = one(helm_release.mysql[*].metadata)
}