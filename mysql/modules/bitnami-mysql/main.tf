resource "helm_release" "mysql" {
  name       = "mysql"
  namespace  = var.namespace
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  version    = var.bitnami_mysql_version

  set {
    name = "global.imageRegistry"
    value = var.image_registry
  }

  set {
    name = "image.registry"
    value = var.image_registry
  }

  set {
    name  = "auth.rootPassword"
    value = var.mysql_root_password
  }

  set {
    name  = "auth.database"
    value = var.mysql_database
  }

  set {
    name  = "auth.username"
    value = var.mysql_user
  }

  set {
    name  = "auth.password"
    value = var.mysql_password
  }

  set {
    name  = "replicaCount"
    value = var.mysql_replica_count
  }

  set {
    name  = "primary.persistence.storageClass"
    value = var.mysql_storage_class
  }

  set {
    name  = "primary.persistence.size"
    value = var.mysql_pvc_size
  }

  dynamic "set" {
    for_each = var.node_selector
    content {
      name  = "primary.nodeSelector.${set.key}"
      value = set.value
    }
  }

  dynamic "set" {
    for_each = var.tolerations
    content {
      name  = "primary.tolerations[${set.key}]"
      value = jsonencode(set.value)
    }
  }

  dynamic "set" {
    for_each = var.affinity
    content {
      name  = "primary.affinity.${set.key}"
      value = jsonencode(set.value)
    }
  }
}
