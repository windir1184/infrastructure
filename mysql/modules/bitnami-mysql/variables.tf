variable "bitnami_mysql_version" {
  description = "value of helm chart version"
  type        = string
  default     = "11.1.4"
}

variable "image_registry" {
  description = "value of image registry, used for China"
  type        = string
  default     = "dhub.kubesre.xyz"
}

variable "namespace" {
  description = "The namespace in which to deploy MySQL"
  type        = string
}

variable "mysql_root_password" {
  description = "Root password for MySQL"
  type        = string
}

variable "mysql_database" {
  description = "Database name to create"
  type        = string
}

variable "mysql_user" {
  description = "Username of new user to create"
  type        = string
}

variable "mysql_password" {
  description = "Password for the new user"
  type        = string
}

variable "mysql_replica_count" {
  description = "Number of MySQL replicas"
  type        = number
  default     = 1
}

variable "mysql_storage_class" {
  description = "Storage class to use for MySQL PVC"
  type        = string
}

variable "mysql_pvc_size" {
  description = "Size of MySQL PVC"
  type        = string
}

variable "node_selector" {
  description = "Node selector for MySQL pods"
  type        = map(string)
  default     = {}
}

variable "tolerations" {
  description = "Tolerations for MySQL pods"
  type        = list(map(string))
  default     = []
}

variable "affinity" {
  description = "Affinity rules for MySQL pods"
  type        = any
  default     = {}
}
