output "mysql_root_password" {
  value = module.mysql.mysql_root_password
}

output "mysql_database" {
  value = module.mysql.mysql_database
}

output "mysql_user" {
  value = module.mysql.mysql_user
}

output "mysql_password" {
  value = module.mysql.mysql_password
}

output "metadata" {
  value = module.mysql.release
}